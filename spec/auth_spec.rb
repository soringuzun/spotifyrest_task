require_relative '../auth'
require 'dotenv'
require 'cgi'
require 'json'
require 'rest-client'
require 'watir'
require 'webdrivers'

describe Auth do
  describe '.login' do
    context 'verify if token is not nil ' do
      it 'The login method should return not nil string' do
        result = Auth.new
        expect(result).not_to be(nil)
      end
    end
  end

  describe '.getToken' do
    context 'verify if token is not nil' do
      it 'The getToken method should return a valid token and not nil result' do
        auth = Auth.new
        url_code = CGI.parse(URI.parse(auth.login).query)['code'][0]
        response = JSON.parse(auth.getToken(url_code))
        token = response['access_token']
        expect(url_code).not_to be(nil)
      end
    end

  end
end

