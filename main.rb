require_relative 'auth'
require 'dotenv'
require 'cgi'
require 'json'
require 'rest-client'
require 'watir'
require 'webdrivers'

# Loading variables from .env file
Dotenv.load

# Initialize auth module
auth = Auth.new

url_code = CGI.parse(URI.parse(auth.login).query)['code'][0]
response = ''
if url_code.nil?
  puts("code for token generation wasn't created successfully. Please try again.")
  exit 2
else
  response = JSON.parse(auth.getToken(url_code))
end

token = response['access_token']

begin
  headers = { 'Content-Type': 'application/json', 'Authorization': "Bearer #{token}" }

  # Get user id
  get_user_id = RestClient.get('https://api.spotify.com/v1/me', headers)
  user_id = JSON.parse(get_user_id)['id']

  # Create test playlist
  body = JSON.generate({ 'name': 'TestPlaylist', 'description': 'New playlist created via SpotifyApi', "public": false })
  create_playlist = RestClient.post("https://api.spotify.com/v1/users/#{user_id}/playlists",
                  body, headers)

  playlist_id = JSON.parse(create_playlist.body)['id']
  # Add some track to TestPlaylist
  add_track_url = "https://api.spotify.com/v1/playlists/#{playlist_id}/tracks"
  payload = JSON.generate(
    {
      'uris': [
        'spotify:track:66hDznGmCAIDVCWEzUVtiu',
        'spotify:track:5XceLjAq6WImzQlpHvgj0R'
      ]
    })
  RestClient.post(add_track_url, payload, headers)
  # Reorder playlist
  reorder_payload = JSON.generate(
    {
      "range_start": 0,
      "insert_before": 2
    }
  )
  RestClient.put(add_track_url, reorder_payload, headers)
  # Delete last track from playlist
  all_tracks = JSON.parse(RestClient.get(add_track_url, headers))['items']
  last_track = all_tracks[all_tracks.size - 1]['track']['uri']

  RestClient::Request.execute(method: :delete, url: add_track_url,
                              payload: JSON.generate({ 'tracks': [{ 'uri': last_track }] }), headers: headers)

rescue Exception => e
  puts e
end


class Playlist
  attr_accessor :id, :name, :description, :owner_name, :spotify_url, :tracks

  def as_json(options={})
    {
      id: @id,
      name: @name,
      description: @description,
      owner_name: @owner_name,
      spotify_url: @spotify_url,
      tracks: @tracks
    }
  end

  def to_json(*options)
    as_json(*options).to_json(*options)
  end
end

class Track
  attr_accessor :id, :name, :artist_name, :album_name, :spotify_url

  def as_json(options={})
    {
      id: @id,
      name: @name,
      artist_name: @artist_name,
      album_name: @album_name,
      spotify_url: @spotify_url
    }
  end

  def to_json(*options)
    as_json(*options).to_json(*options)
  end
end

getPlaylist = JSON.parse(RestClient.get("https://api.spotify.com/v1/playlists/#{playlist_id}", headers).body)


final = Playlist.new
final.id = getPlaylist['id']
final.name = getPlaylist['name']
final.description = getPlaylist['description']
final.owner_name = getPlaylist['owner']['display_name']
final.spotify_url = getPlaylist['external_urls']['spotify']
final.tracks = []

getPlaylist['tracks']['items'].each  do |track|
  tr = Track.new
  tr.id = track['track']['id']
  tr.name = track['track']['name']
  tr.artist_name = track['track']['artists'][0]['name']
  tr.album_name = track['track']['album']['name']
  tr.spotify_url = track['track']['external_urls']['spotify']

  final.tracks << tr
end


puts JSON.pretty_generate(final)
