class Auth
  def login
    @browser = Watir::Browser.new :chrome, headless: true,
                                          options: { args: ['--user-data-dir=./browserData']}
    @authorize_url = "https://accounts.spotify.com/en/authorize?client_id=#{ENV['CLIENT_ID']}"\
                     "&redirect_uri=#{ENV['REDIRECT_URI']}&response_type=code&scope=playlist-modify-private"
    @browser.goto(@authorize_url)
    if @browser.url == @authorize_url
      @browser.button(id: 'auth-accept').wait_until(&:present?).click
      @auth_code = @browser.url
    elsif @browser.url.start_with?('https://www.soguzu.me/?code')
      @auth_code = @browser.url
    else
      @browser.text_field(id: 'login-username').wait_until(&:present?).set ENV['SPOTIFY_LOGIN']
      @browser.text_field(id: 'login-password').wait_until(&:present?).set ENV['SPOTIFY_PASSWORD']
      @browser.button(id: 'login-button').click
      sleep(5)
      if @browser.button(id: 'auth-accept').exist?
        @browser.button(id: 'auth-accept').click
        @auth_code = @browser.url
      else
        @auth_code = @browser.url
      end
    end
    @browser.close
    return @auth_code
  rescue StandardError => e
    puts e.message
  end

  def getToken(code)
    payload = { 'grant_type': 'authorization_code',
                'code': code,
                'redirect_uri': ENV['REDIRECT_URI_DECODED'],
                'client_id': ENV['CLIENT_ID'],
                'client_secret': ENV['CLIENT_SECRET'] }

    client = RestClient.post('https://accounts.spotify.com/api/token',
                             payload,
                             headers = { "content_type": 'application/x-www-form-urlencoded' })
    return client
  rescue StandardError, TimeOutError => e
    puts e.message
  end

end
